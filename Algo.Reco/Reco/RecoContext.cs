﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Algo
{
    public class RecoContext
    {
        struct KeyPair
        {
            public readonly User U1;
            public readonly User U2;

            public KeyPair( User u1, User u2 )
            {
                U1 = u1;
                U2 = u2;
            }

            public override bool Equals(object obj)
            {
                if( !(obj is KeyPair) ) return false;
                KeyPair o = (KeyPair)obj;
 	            return (U1 == o.U1 && U2 == o.U2) || (U1 == o.U2 && U2 == o.U1);
            }

            public override int GetHashCode()
            {
                return U1.UserID ^ U2.UserID;
            }
        }

        readonly Dictionary<KeyPair,double> _similarities;

        public User[] Users { get; private set; }
        public Movie[] Movies { get; private set; }

        public RecoContext()
        {
            _similarities = new Dictionary<KeyPair, double>();
        }

        public void LoadFrom( string folder )
        {
            Users = User.ReadUsers( Path.Combine( folder, "users.dat" ) );
            Movies = Movie.ReadMovies( Path.Combine( folder, "movies.dat" ) );
            User.ReadRatings( Users, Movies, Path.Combine( folder, "ratings.dat" ) );
        }

        public double SimilarityPearson( User u1, User u2 )
        {
            if( u1 == u2 ) return 1.0;
            double result;
            if( _similarities.TryGetValue( new KeyPair( u1, u2 ), out result ) )
            {
                return result;
            }
            result = DoComputeSimilarity( u1, u2 );
            _similarities.Add( new KeyPair( u1, u2 ), result );
            return result;
        }

        private static double DoComputeSimilarity( User u1, User u2 )
        {
            IEnumerable<Movie> common = u1.Ratings.Keys.Intersect( u2.Ratings.Keys );
            int count = common.Count();
            if( count < 2 ) return 0;
            double sum1 = 0;
            double sum2 = 0;
            double sumProd = 0;
            double sumSquare1 = 0;
            double sumSquare2 = 0;
            foreach( var m in common )
            {
                int r1 = u1.Ratings[m];
                int r2 = u2.Ratings[m];
                sum1 += r1;
                sum2 += r2;
                sumProd += r1 * r2;
                sumSquare1 += (r1 * r1);
                sumSquare2 += (r2 * r2);
            }
            double numerator = sumProd - (sum1 * sum2) / count;
            double denominator = Math.Sqrt( (sumSquare1 - (sum1 * sum1) / count) * (sumSquare2 - (sum2 * sum2) / count) );
            if( denominator < Double.Epsilon ) return 0;
            double s = numerator / denominator;
            if( s < -1 ) return -1;
            if( s > 1 ) return 1;
            return s;
        }


    }
}
