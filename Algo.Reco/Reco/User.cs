﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Algo
{
    public partial class User
    {
        internal static string[] CellSeparator = new string[] { "::" };

        UInt16 _userId;
        byte _age;
        bool _male;
        string _occupation;
        string _zipCode;
        IReadOnlyList<BestFUser> _bestFUsers; 

        /// <summary>
        /// User information is in the file "users.dat" and is in the following
        /// format:
        /// UserID::Gender::Age::Occupation::Zip-code
        /// </summary>
        /// <param name="line"></param>
        User( string line )
        {
            string[] cells = line.Split( CellSeparator, StringSplitOptions.None );
            _userId = UInt16.Parse( cells[0] );
            _male = cells[1] == "M";
            _age = Byte.Parse( cells[2] );
            _occupation = String.Intern( cells[3] );
            _zipCode = String.Intern( cells[4] );
            Ratings = new Dictionary<Movie, int>();
        }

        static public User[] ReadUsers( string path )
        {
            List<User> u = new List<User>();
            using( TextReader r = File.OpenText( path ) )
            {
                string line;
                while( (line = r.ReadLine()) != null ) u.Add( new User( line ) );
            }
            return u.ToArray();
        }

        static public void ReadRatings( User[] users, Movie[] movies, string path )
        {
            using( TextReader r = File.OpenText( path ) )
            {
                string line;
                while( (line = r.ReadLine()) != null )
                {
                    string[] cells = line.Split( CellSeparator, StringSplitOptions.None );
                    int idUser = int.Parse(cells[0]);
                    int idMovie = int.Parse( cells[1] );
                        users.FirstOrDefault(m => m.UserID == idUser ).Ratings.Add( movies.FirstOrDefault(m =>m.MovieID == idMovie), int.Parse( cells[2] ) );
                }
            }
        }

        public int UserID { get { return (int)_userId; } set { _userId = (UInt16)value; } }

        public bool Male { get { return _male; } }

        public int Age { get { return (int)_age; } }
        
        public string Occupation { get { return _occupation; } }

        public string ZipCode { get { return _zipCode; } }

        public Dictionary<Movie, int> Ratings { get; private set; }

        class BestFUserComparer : IComparer<BestFUser>
        {
            public int Compare( BestFUser x, BestFUser y )
            {
                double delta = Math.Abs( x.Similarity ) - Math.Abs( y.Similarity );
                if( delta > 0 ) return 1;
                if( delta < 0 ) return -1;
                return 0;
            }
        }
        readonly static BestFUserComparer _fUserComparer = new BestFUserComparer();

        public IReadOnlyList<BestFUser> GetBestFUsers( RecoContext c )
        {
            if( _bestFUsers == null )
            {
                var bestK = new BestKeeper<BestFUser>( 300, _fUserComparer );
                foreach( User u in c.Users )
                {
                    if( u != this )
                    {
                        double s = c.SimilarityPearson( this, u );
                        bestK.AddCandidate( new BestFUser( u, s ) );
                    }
                }
                _bestFUsers = bestK.GetBest();
            }
            return _bestFUsers;
        }

        public IReadOnlyList<Movie> RecommendedMovies( RecoContext c, int count )
        {
            return GetBestFUsers( c )
                .SelectMany( b => b.FUser.Ratings
                                            .Where( k => !this.Ratings.ContainsKey( k.Key ) )
                                            .Select( k => new { Movie = k.Key, W = k.Value * b.Similarity } ) )
                .GroupBy( mw => mw.Movie )
                .Select( g => new { Movie = g.Key, SumW = g.Sum( mw => mw.W ) / g.Count() } )
                .OrderByDescending( candidate => candidate.SumW )
                .Select( candidate => candidate.Movie )
                .Take( count )
                .ToArray();
        }



        public double EuclidianDistanceTo( User u )
        {
            return EuclidianDistance( this, u );
        }

        public static double EuclidianSimilarity( User u1, User u2 )
        {
            double d = EuclidianDistance( u1, u2 );
            if( Double.IsNaN( d ) ) return 0;
            return 1 / (1 + d);
        }

        public static double EuclidianDistance( User u1, User u2 )
        {
            if( u1 == u2 ) return 0;
            double result = 0;
            var r1 = u1.Ratings;
            var r2 = u2.Ratings;
            int commonMovieCount = 0;
            foreach( KeyValuePair<Movie,int> rating1 in r1 )
            {
                int note2;
                if( r2.TryGetValue( rating1.Key, out note2 ) )
                {
                    int note1 = rating1.Value;
                    result += Math.Pow( note1 - note2, 2 );
                    ++commonMovieCount;
                }
            }
            if( commonMovieCount == 0 ) return Double.NaN;
            return Math.Sqrt( result );
        }

 
    }
    

}
