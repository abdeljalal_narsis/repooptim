﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algo
{
    public class BestKeeper<T>
    {
        readonly IComparer<T> _comparer;
        readonly int _count;
        readonly List<T> _allCandidates;

        public BestKeeper( int count, IComparer<T> comparer )
        {
           
            if( count <= 0 ) throw new ArgumentException( "Must be greater than 0.", "count" );
            if( comparer == null ) throw new ArgumentNullException( "comparer" );
            _count = count;
            _comparer = comparer;
            _allCandidates = new List<T>();
        }

        public void AddCandidate( T candidate )
        {
            _allCandidates.Add( candidate );
        }
        
        public IReadOnlyList<T> GetBest()
        {
            return _allCandidates.OrderBy( x => x, _comparer ).Take( _count ).ToArray();
        }
    }

}
